using tugasEmail.Models;

namespace tugasEmail.Services.EmailService
{
    public interface IEmailService
    {
        void SendEmail(EmailDTO request);
    }
}

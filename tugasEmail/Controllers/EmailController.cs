﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tugasEmail.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit.Text;
using tugasEmail.Services.EmailService;

namespace tugasEmail.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {

        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }


        [HttpPost("DailyNotes")]
        public IActionResult SendEmail(EmailDTO request)
        {

            _emailService.SendEmail(request);

            return Ok("Successfully");
        }
    }


}
